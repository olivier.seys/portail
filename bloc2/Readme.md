Bloc 2 - Algorithmique
======================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Benoit Papegay,
Bruno Bogaert,
Éric Wegrzynowski,
Laetitia Jourdan,
Lucien Mousin,
Marie-Émilie Voge,
Patricia Everaere,
Philippe Marquet,
Sophie Tison


Séance 1 - 10 mai
=================

* [Activité d'informatique sans ordinateur - les tris](tri-sans-ordi/Readme.md)
  - [le déroulé de l'activité](tri-sans-ordi/deroule.md)
* [Reconnaître et programmer les tris](tris_anonymes/Readme.md)

Ressources sur les tris
-----------------------

* Présentation et animation des tris sur le site
  [lwh.free.fr/](http://lwh.free.fr/pages/algo/tri/tri.htm)
* Des algorithmes de tri visualisés avec des danses folkloriques sur la
  [chaîne vidéo AlgoRythmics](https://www.youtube.com/user/AlgoRythmics/videos)

Séance 2 - 15 mai
=================

* [Synthèse sur les tris exprimés en Français](tri-sans-ordi-correction/readme.md)
* [Analyse en temps d'exécution des tris](analyse_tris/Readme.md)

Pour le 17 juin
===============

* [Analyse en temps d'exécution des tris](analyse_tris/Readme.md#pour-la-prochaine-fois-17-juin)
  - étude expérimentale de la méthode `sort` de Python
  - séance élèves temps d'exécution de différents algorithmes,
    illustration sur les algorithmes de tri

Séance 3 - 17 juin
==================

* Analyse théorique des algorithmes, illustration sur les algorithmes de tri
  - cours
* Analyse de complexité d'algorithmes
  - travaux dirigés

Séance 4 - 18 juin
==================

* Correction des algorithmes
  - cours
* Algoritrhmes gloutons
  - travaux dirigés
  
Séance 5 - 21 juin
==================

* Correction des algorithmes
  - cours, suite 
* Algorithmes gloutons
  - travaux pratiques

Séance 6 et 7 - semaine du 1er juillet
======================================

* Algorithme des k plus proches voisins
  - travaux dirigés puis pratiques

